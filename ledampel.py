#!/usr/bin/python3
# (C) Hermann Lauer 2020
# can be distributed under the GPL V3 or later
"""NAME
        %(prog)s - <one line short description>

SYNOPSIS
        %(prog)s [--help]

DESCRIPTION
        none

FILES
        none

SEE ALSO
        nothing

DIAGNOSTICS
        none

BUGS
        none

CREDITS
        Tommi2Day, https://github.com/datenschuft/SMA-EM.git

AUTHOR
        Hermann.Lauer@uni-heidelberg.de
        {version}
"""

import bananaio

class AmpelDummy(object):
  def write(s,val): pass

class AmpelIO(bananaio.gpio):
  
  def __init__(s):
    super().__init__(s,needatomic=True) #need frobbing bits
    for g in ('D16','D17','D18'):
      s.setDir(g,1)                     #outputs
    s._write2=s.write

  def write2(s,val):
    s._write2('D16',val,0x7)

  def run(s,vals):
    import time

    supply,power=vals

    bits=0
    sleep=0
    if supply<0:
      try:
        ratio=power/(power-supply)
        sleep=ratio-0.1
        if power==power: bits|=4        #test for nan
        bits2=bits
        if sleep>0: bits|=2
      except TypeError as e: print(e)
    elif supply>6800: bits|=1
    else: bits|=2
    s.write2(bits)
    if sleep>0:
        time.sleep(sleep)
        s.write2(bits2)
        print(sleep,supply,power,ratio)

led=AmpelIO()
if __name__=="__main__":
  import sys, socket, syslog, os
  syslog.openlog(os.path.basename(sys.argv[0]), 0, syslog.LOG_DAEMON)

  HOST = ''                 # Symbolic name meaning the local host
  PORT = 4331               # Arbitrary non-privileged port (cmd: 4333)
  s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  try:
    s.bind((HOST, PORT))
  except socket.error as errex:
    syslog.syslog(syslog.LOG_ERR,"bind failed: %s"%(errex[1]))
    sys.stderr.write("bind failed: %s\n"%(errex[1]))
    sys.exit(1)

#  os.environ["PYTHONINSPECT"] = "1"       #allow interactive debug

  pktcnt=1
  power=None
  while True:
    try:
      data,iaddr = s.recvfrom(1504)
      if data==b"\x11":          #got a reset cmd
        led.reset()
        continue
      st=data
      sp=st.split(None,1)
      if len(sp)>1: power=float(sp[1])
#        print(sp)
      led.run((int(sp[0])/10,power))
      print(iaddr,data)
      pktcnt+=1
    except IOError as e:
      print(e)
