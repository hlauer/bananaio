#!/usr/bin/python3
#
# (C) Hermann Lauer 20016-2017
# can be distributed under the GPL V3 or later

import mmapio

class CCU(mmapio.mmapio):
    IOBase=0x01C20000
    Size=0x200
    Count=1
    
    def __init__(s,idx=0,needatomic=False):
        """if needatomic is true, loading the c ext for frobbing bits must succeed"""
        s.IOSize=s.Size*s.Count
        super().__init__(needatomic)
        s.calcoffsize(s.Size*idx)       #set internal offset to index

    def info(s):
        """print interesting registers"""
        print("PLL2_CFG {:x}, PLL2_TUN {:x}, IIS0_CLK {:x}, IIS1_CLK {:x}".format(
          s._read(8), s._read(0xc), s._read(0x8b), s._read(0xd8)), end=', ')
        print("IIS2_CLK {:x}, THS_CLK_REG {:x}".format(
            s._read(0xdc), s._read(0x7c)))
        s.freq(s._read(0))
        
    def freq(s,r):
        P=1<<((r>>16)&3)
        N=((r>>8)&0x1f)+1
        K=((r>>4)&3)+1
        M=(r&3)+1
        clk=((24000000*K)*N)/(M*P)
        print('{:x} P:{} N:{} K:{} M:{} {}Hz'.format(r,P,N,K,M,clk))
#########################################

if __name__ == '__main__':
    ccu=CCU()
    ccu.info()
#    u._write(0xa4,0x20,0x30)    #try to invert RX (description IR)
#    u.info()
