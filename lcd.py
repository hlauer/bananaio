import bananaio

class LcdDummy(object):
  """PI 266-269 D4-D7
     PI 270     R/S     H:data, L:cmd
     PI 272     E"""
  E=(1<<7)
  RS=(1<<5)
  def write(s,val): pass

class LcdIO(bananaio.gpio):
  """266-269    D4-D7
     270        R/S     H:data, L:cmd
     272        E"""
  E=(1<<6)
  RS=(1<<4)
  
  def __init__(s):
    super().__init__(s,needatomic=True) #need frobbing bits
    for g in (266,267,268,269,270,272):
      s.setDir(g,1)                     #outputs
    s._write2=s.write

  def write2(s,val):
    s._write2(266,val,0x5f)

####################################################################
class LcdPort(object):
  """access via port:
#LP03: E
#LP04: R/W	H:read, L:write
#LP05: RS	H:data, L:cmd
#LP06-LP09: D4-D7
"""

  def __init__(s,port):

    s.port=port
    s.porwrite=port.write2
    s.delay=0.00022			#time to wait for setteling: 220 ns
    s.E=port.E
    s.RS=port.RS
    s.reset()
		
  def reset(s):
    """do a full reinit of the lcd display"""
    import time

    s.resync()

    s.cmd(0x0c)			#DISPLAY ON/OFF: on, cursor off, blinking off
    s.cmd(0x01)			#DISPLAY CLEAR
    time.sleep(0.00164)
    s.cmd(0x06)			#ENTRY MODE SET: cursor increase, no shift

  def resync(s):
    """resync for 4 bit mode
    got into 8 bit mode, where resync is possible, then change to 4 bit mode again"""

    s.cmd(0x33)			#SET FUNCTION: 8bit (2 times)
#	# do we need a additional 0x3x byte here ? not shure
#	s.smbus.write_byte(0x24,0x20)	#SET FUNCTION: 4bit (as 8 bit command)
    s.cmd(0x32)			#SET FUNCTION: 8bit, #SET FUNCTION: 4bit (as 8 bit command)
    s.cmd(0x28)	#28		#SET FUNCTION: 4bit, 2 line display, 5x7 dots

  def write(s,st):
    for v in st:
      s.out(v)

  def goto(s,li,col):
    """set cursor to coordinate of display"""
    adr=col
    if li&1: adr+=0x40
    if li>1: adr+=20
    s.cmd(0x80|adr)

  def cmd(s,val):
    import time

    hn= (val & 0xf0)>>4
    s.porwrite(hn)
    s.porwrite(hn|s.E)	# E
    time.sleep(s.delay)
    s.porwrite(hn)

#    time.sleep(0.00001)
    hn= (val & 0xf)
    s.porwrite(hn)
    s.porwrite(hn|s.E)	# E
    time.sleep(s.delay)
    s.porwrite(hn)

  def out(s,val):
    import time

    hn= ((val & 0xf0)>>4) | s.RS
    s.porwrite(hn)
    s.porwrite(hn|s.E)	# E
    time.sleep(s.delay)
    s.porwrite(hn)

#    time.sleep(0.00001)
    hn= (val & 0xf) | s.RS
    s.porwrite(hn)
    s.porwrite(hn|s.E)	# E
    time.sleep(s.delay)
    s.porwrite(hn)

  def disenable(s,val):
    """if None, enable, otherwise set value and disable"""
    if val==None:
      s.porwrite=s.port.write
      return
    s.porwrite(val)
    s.porwrite=lambda x: None

##############################
class LCDPort(LcdPort):
  """add some higlevel funktions"""

  def gowrite(s,st,goto=None):
    if goto: s.goto(goto[0],goto[1])
    s.write(st)

i=LcdIO()
lcd=LCDPort(i)
if __name__=="__main__":
  import sys, socket, syslog, os
  syslog.openlog(os.path.basename(sys.argv[0]), 0, syslog.LOG_DAEMON)

  HOST = ''                 # Symbolic name meaning the local host
  PORT = 4332               # Arbitrary non-privileged port (cmd: 4333)
  s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  try:
    s.bind((HOST, PORT))
  except socket.error as errex:
    syslog.syslog(syslog.LOG_ERR,"bind failed: %s"%(errex[1]))
    sys.stderr.write("bind failed: %s\n"%(errex[1]))
    sys.exit(1)

  os.environ["PYTHONINSPECT"] = "1"       #allow interactive debug

  pktcnt=1
  while True:
    try:
      data,iaddr = s.recvfrom(1504)
      if data==b"\x11":          #got a reset cmd
        lcd.reset()
        continue
      st=data
      if pktcnt%10==0:
        if pktcnt%1440==0:  lcd.reset()
        else:  	    	    lcd.resync()
      for i in range(4):
        sp=st.split(b"\n",1)
#        print(sp)
        if len(sp)==1:
          sp=(st[:20],st[20:])
        li,st=sp
        lcd.gowrite(li,(i,0))
#        print(i,li,st)
        if not st: break
      print(iaddr,data)
      pktcnt+=1
    except IOError as e:
      print(e)
