#!/usr/bin/python3
#
# (C) Hermann Lauer 20016-2018
# can be distributed under the GPL V3 or later

import mmapio

class THS(mmapio.mmapio):
    IOBase=0x01C24C00
    Size=0x400
    Count=1
    
    def __init__(s,idx=0,needatomic=False):
        """if needatomic is true, loading the c ext for frobbing bits must succeed"""
        s.IOSize=s.Size*s.Count
        super().__init__(needatomic)
        s.calcoffsize(s.Size*idx)       #set internal offset to index

    def info(s):
        """print interesting registers"""
        print("THS_CTRL0 {:x}, THS_CTRL1 {:x}, THS_INTC {:x}, THS_INTS {:x}".format(
          s._read(0), s._read(0x40), s._read(0x44), s._read(0x48)), end=', ')
        print("THS0_ALARM_CTRL {:x}, THS1_ALARM_CTRL {:x}, THS0_SHUTDOWN_CTRL {:x}, THS1_SHUTDOWN_CTRL {:x}".format(
          s._read(0x50), s._read(0x54), s._read(0x60), s._read(0x64)), end=', ')
        print("THS_FILTER {:x}, THS0_1_CDATA {:x}, THS0_DATA {:x}, THS1_DATA {:x}".format(
          s._read(0x70), s._read(0x74), s._read(0x80), s._read(0x84)))

        print("cpu {}C, gpu {}C".format(
          s.tempconv(s._read(0x80)), s.tempconv(s._read(0x84))))

    def init(s):
        s.store(0,0x1df)
        s.store(0x40,0x01df0000)
        s.store(0x48,0x333)
        s.store(0x70,6)
        s.store(0x74,0x80107ff)
        s.store(0x60,0x4E80000)
        s.store(0x64,0x4E80000)

        s.store(0x44,0x3a070)

        s._write(0x40,3,3)

    def tempconv(s,reg):
        """calc temp"""
        return ((250*1024*1024) - reg*(0.1125*1024*1024))/(1<<20)

    def temp(s,idx=0):
        return s.tempconv(s._read(0x80+4*(idx==1))) 

#########################################
def tempsend(ths,host,port=2003):
    import socket, time
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect((host, port))

    while True:
        try:
            cpu,gpu=ths.temp(0),ths.temp(1)
            pkt='temp {} {}'.format(cpu,gpu)
            s.send(pkt.encode(errors='replace'))
            print(pkt)
            if cpu>70: time.sleep(5)
            else: time.sleep(30)
        except Exception as e:
            syslog.syslog(syslog.LOG_WARNING,"udp send: {}".format(e))
            time.sleep(60)

if __name__ == '__main__':
    ths=THS()

    from ccu import CCU
    ccu=CCU()
    ccu.info()
    print("BUS_CLK_GATING_REG2(68/8) {:x} THS_CLK_REG(74) {:x} BUS_SOFT_RST_REG3(2d0/8) {:x}".format(
          ccu._read(0x68), ccu._read(0x74), ccu._read(0x2d0)))
    ths.info()

    if not ccu._read(0x2d0) & (1<<8):
        print("THS hold in reset, starting and initializing")
#THS_CLK
        ccu.store(0x74,0x80000000)
#deassert reset
        ccu._write(0x2d0,1<<8,1<<8)
#ungate THS_CLK
        ccu._write(0x68,1<<8,1<<8)
        ths.init()

    ths.temp()
