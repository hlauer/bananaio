#!/usr/bin/python3
#
# (C) Hermann Lauer 20016-2017
# can be distributed under the GPL V3 or later

# bananaPi/Pro layout
#	-1, -1, //1, 2     3.3 , 5
#	53, -1, //3, 4  I2C-SDA, 5
#	52, -1, //5, 6  I2C-SCL, GND
#	259, 224, //7, 8        UART-TX
#	-1, 225, //9, 10   GND, UART-RX
#	275, 226, //11, 12
#	274, -1, //13, 14
#	273, 244, //15, 16
#	-1, 245, //17, 18  3.3
#	268, -1, //19, 20       PI, GND
#	269, 272, //21, 22      PI, PI
#	267, 266, //23, 24      PI, PI
#	-1, 270, //25, 26       GND,PI
#
#       272-275 or 266-270
#	
#	257, 256, //27, 28
#	35, -1, //29, 30
#	277, 276, //31, 32      PI21/RX7, PI20/TX7
#	45, -1, //33, 34
#	39, 38, //35, 36        LRCLK,BCLK
#	37, 44, //37, 38
#	-1, 40, //39, 40        DO0

# J12 (UART and Power source)
# 1, 2    -, -   //        5V, 3.3V
# 3, 4  229, 277 // PH5/EINT5, PI21/UART7_RX
# 5, 6  227, 276 // PH3/EINT3, PI20/UART7_TX
# 7, 8    -, -   //       GND, GND

GPIO_BASE_BP    = 0x01C20000
SUNXI_GPIO_BASE = 0x01C20800
BLOCK_SIZE = 4*1024
#CONFIG_STRICT_DEVMEM

class gpio(object):
  offset=SUNXI_GPIO_BASE-GPIO_BASE_BP   #calc offset in BLOCK aligned mmap

  def __init__(s,bank=0,needatomic=False):
    """if needatomic is true, loading the c ext for frobbing bits must succeed"""
    s.bank=bank
    s.gpiommap()
    try:
     import atomicfrob
     s._frob=atomicfrob.frob
     s._write=s._writeatomic
    except KeyError as e:
      if needatomic: raise e
    
  def gpiommap(s):
    import os, mmap
    fd = os.open("/dev/mem", os.O_RDWR|os.O_SYNC)
    s.mem = mmap.mmap( fd, BLOCK_SIZE, mmap.MAP_SHARED, mmap.PROT_READ|mmap.PROT_WRITE, offset=GPIO_BASE_BP)
#    s.mem = mmap.mmap( fd, BLOCK_SIZE, mmap.MAP_PRIVATE, mmap.PROT_READ|mmap.PROT_WRITE, offset=GPIO_BASE_BP)
    return s.mem

  def _read(s,off=0x10):
    from struct import unpack
    off+=s.offset
    return unpack("L",s.mem[off:off+4])[0]

  def _write(s,off,val,mask):
    from struct import pack,unpack
    off+=s.offset
    reg=unpack("L",s.mem[off:off+4])[0]
    nreg=(reg&~mask)|val
    s.mem[off:off+4]=pack("L",nreg)
    print("addr {:x}: set {:x} old {:x} val {:x} mask {:x}".format(off,nreg,reg,val,mask))

  def _writeatomic(s,off,val,mask):
    """use a c extension to avoid problems with python threading - i.e. this way should be 
python threadsave"""
    off+=s.offset
    res=s._frob(val,mask,off,s.mem)
#    print("addr {:x}: set {:x} val {:x} mask {:x}".format(off,res,val,mask))

  def write(s,pin,val,mask):
    """write a bank containig pins as 32 bit - mask must be inside"""
    bank,idx=s.calcphy(pin)
#    print(bank,idx)
    return s._write(bank+0x10,val<<idx,mask<<idx)

  def writebit(s,pin,val): return s.write(pin,val,1)

  def read(s,pin):
    """read bank containig pin as 32 bit"""
    bank,idx=s.calcphy(pin)
    return s._read(bank+0x10)

  def readbit(s,pin):
    """read pin"""
    bank,idx=s.calcphy(pin)
    return (s._read(bank+0x10)>>idx)&1
  
  def calcphy(s,pin):
    """return bank offset (*0x24), index"""
    if type(pin)==type(''):
      if pin.startswith('P'): pin=pin[1:]
      bank=ord(pin[0])-ord('A')
      index=int(pin[1:])
    else:
      bank = pin >> 5
      index = pin - (bank << 5)
    if bank<0 or bank>8: raise ValueError
    return bank * 36,index

  def pininfo(s,pin):
    bank,idx=s.calcphy(pin)
    bank=int(bank/0x24)
    return bank*32+idx,"P{}{}".format(chr(bank+65),idx),bank,idx

  def calcselmask(s,pin):
    """for configure registers:
return addr offset, idx shift, mask"""

    bank,pin=s.calcphy(pin)
    reg = pin>>3
    idx = pin&7
    return reg*4+bank, idx*4, 7<<(idx*4)

  def getDir(s,pin):
    reg,idx,mask=s.calcselmask(pin)
    print("reg {:x} idx {:x} mask {:x}".format(reg,idx,mask))
    return (s._read(reg)&mask)>>idx

  def setDir(s,pin,dir):
    reg,idx,mask=s.calcselmask(pin)
#    print("reg {:x} idx {:x} mask {:x}".format(reg,idx,mask))
    s._write(reg,dir<<idx,mask)

  def calcdrvmask(s,pin):
    """for current driver registers:
return addr offset, idx shift, mask"""

    bank,pin=s.calcphy(pin)
    reg = pin>>4
    idx = pin&0xf
    return reg*4+bank, idx*2, 3<<(idx*2)

  def getDrv(s,pin):
    reg,idx,mask=s.calcdrvmask(pin)
    print("reg {:x} idx {:x} mask {:x}".format(reg+0x14,idx,mask))
    return (s._read(reg+0x14)&mask)>>idx

  def setDrv(s,pin,drv):
    reg,idx,mask=s.calcdrvmask(pin)
    s._write(reg+0x14,drv<<idx,mask)

  def getPull(s,pin):
    reg,idx,mask=s.calcdrvmask(pin)
    print("reg {:x} idx {:x} mask {:x}".format(reg+0x1c,idx,mask))
    return (s._read(reg+0x1c)&mask)>>idx

  def setPull(s,pin,pull):
    reg,idx,mask=s.calcdrvmask(pin)
    s._write(reg+0x1c,pull<<idx,mask)

  def showdir(s,idx=0):
    r=s._read(idx)
    for i in range(32//3):
      print("{}\t{}".format(i,r&7))
      r>>=3

def reset():
  import time
  g.setDir('B22',1)
  g.setPull('B22',2)
  print('Wait 10s to drain console before reconnecting')
  time.sleep(10)
  g.setPull('B22',0)
  g.setDir('B22',2)

#########################################

if __name__ == '__main__':
  g=gpio()
#g.getDir(38)  #I2S_BCLK
#g.getDir(39)  #I2S_LRCLK
#g.getDir(40)  #I2S_DO0
#  g.setDir(38,2)  #I2S_BCLK
#  g.setDir(39,2)  #I2S_LRCLK
#  g.setDir(40,2)  #I2S_DO0
#getPull->0,getDrv->1
