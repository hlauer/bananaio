#!/usr/bin/python3
#
# (C) Hermann Lauer 20016-2017
# can be distributed under the GPL V3 or later

import mmapio

class I2S(mmapio.mmapio):
    IOBase=0x01C22000   #DA1, DA0=0x01C22400
    Size=0x400
    Count=2
    
    def __init__(s,idx=1,needatomic=False):
        """if needatomic is true, loading the c ext for frobbing bits must succeed"""
        s.IOSize=s.Size*s.Count
        super().__init__(needatomic)
        s.calcoffsize(s.Size*idx)       #set internal offset to index

    def info(s):
        """print interesting registers"""
        print("DA_CTL {:x}, DA_FAT0 {:x}, DA_FAT1 {:x}, DA_FCTL {:x}".format(
          s._read(0), s._read(4), s._read(8), s._read(0x14)), end=', ')
        print("DA_FSTA {:x}, DA_INT {:x}, DA_ISTA {:x}, DA_CLKD {:x}".format(
          s._read(0x18),s._read(0x1c),s._read(0x20),s._read(0x24)), end=', ')
        print("DA_TXCNT {:x}, DA_RXCNT {:x}, DA_TXCHSEL {:x}, DA_TXCHMAP {:x}".format(
          s._read(0x28),s._read(0x2c),s._read(0x30),s._read(0x34)), end=', ')
        print("DA_RXCHSEL {:x}, DA_RXCHMAP {:x}".format(
          s._read(0x38),s._read(0x3c)))

    def setIR(s,RXinv=0,TXinv=0):
      val=(RXinv<<5)|(TXinv<<4)
      return s._write(0xa4,val,3<<4)
      
#########################################

if __name__ == '__main__':
    i2s=I2S()
    i2s.info()
#    u._write(0xa4,0x20,0x30)    #try to invert RX (description IR)
#    u.info()
