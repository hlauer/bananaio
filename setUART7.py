import bananaio

class LcdIO(bananaio.gpio):
  """266-269    D4-D7
     270        R/S     H:data, L:cmd
     272        E"""
  E=(1<<6)
  RS=(1<<4)
  
  def __init__(s):
    super().__init__(s,needatomic=True) #need frobbing bits
    for g in (266,267,268,269,270,272):
      s.setDir(g,1)                     #outputs
    s._write2=s.write

print("g.setPull(277,1)")
if __name__ == '__main__':
  g=bananaio.gpio()
  g.setDir(276,1)                               #PI20 (UART7_TX) output
  print("PI20 Drive strength {}".format(g.getDrv(276)))
  print("setting {}".format(g.setDir(276,1)))   #PINCTRL_20_MA
  print("settingPull {}".format(g.setPull(277,0)))	#1:pull up 2:pull down
  print("PI21 Pull {}".format(g.getPull(277)))
  g.setDir(276,3)                               #UART7_TX
