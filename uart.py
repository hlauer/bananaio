#!/usr/bin/python3
#
# (C) Hermann Lauer 20016-2017
# can be distributed under the GPL V3 or later
GPIO_BASE_BP    = 0x01C20000
SUNXI_GPIO_BASE = 0x01C20800
BLOCK_SIZE = 4*1024

class mmapio(object):
  """base class to allow direct io accesses via mmap
  """
  offset = SUNXI_GPIO_BASE-GPIO_BASE_BP   #calc offset in BLOCK aligned mmap
  IOBase = GPIO_BASE_BP
  IOSize = BLOCK_SIZE

  def __init__(s,needatomic=False):
    """if needatomic is true, loading the c ext for frobbing bits must succeed"""
    s.gpiommap()
    try:
     import atomicfrob
     s._frob=atomicfrob.frob
     s._write=s._writeatomic
    except KeyError as e:
      if needatomic: raise e
    
  def gpiommap(s):
    import os, mmap
    fd = os.open("/dev/mem", os.O_RDWR|os.O_SYNC)
    s.mem = mmap.mmap(fd, s.IOSize, mmap.MAP_SHARED, mmap.PROT_READ|mmap.PROT_WRITE, offset=s.IOBase)
    print("addr {:x}: size {:x} len {:x}".format(s.IOBase,s.IOSize,len(s.mem)))
    return s.mem

  def _read(s,off=0x10):
    from struct import unpack
    off+=s.offset
    val=unpack("L",s.mem[off:off+4])[0]
    print("addr {:x}: res {:x}".format(off,val))
    return val
    
  def _write(s,off,val,mask):
    from struct import pack,unpack
    off+=s.offset
    reg=unpack("L",s.mem[off:off+4])[0]
    nreg=(reg&~mask)|val
    s.mem[off:off+4]=pack("L",nreg)
    print("addr {:x}: set {:x} old {:x} val {:x} mask {:x}".format(off,nreg,reg,val,mask))

  def _writeatomic(s,off,val,mask):
    """use a c extension to avoid problems with python threading - i.e. this way should be 
python threadsave"""
    off+=s.offset
    res=s._frob(val,mask,off,s.mem)
#    print("addr {:x}: set {:x} val {:x} mask {:x}".format(off,res,val,mask))

class uart(mmapio):
    IOBase=0x01C28000
    Size=0x400
    Count=8
    
    def __init__(s,idx=7,needatomic=False):
        """if needatomic is true, loading the c ext for frobbing bits must succeed"""
        s.IOSize=s.Size*s.Count     #1Ktwo pages here
        super().__init__(needatomic)
        s.offset=s.Size*idx

    def info(s):
        """print interesting registers"""
        print("FIFO_CTL {:x}, LCR {:x}, MCR {:x}, LSR {:x}".format(s._read(8),s._read(0xc),
                                                                   s._read(0x10),s._read(0x14)),end=' ')
        print("MSR {:x}, USR {:x}, RFL {:x}, HALT {:x}".format(s._read(0x18),s._read(0x7c),
                                                                s._read(0x84),s._read(0xa4)))

    def setIR(s,RXinv=0,TXinv=0):
      val=(RXinv<<5)|(TXinv<<4)
      return s._write(0xa4,val,3<<4)
      
#########################################

if __name__ == '__main__':
    u=uart()
    u.info()
    u._write(0xa4,0x20,0x30)    #try to invert RX (description IR)
    u.info()
